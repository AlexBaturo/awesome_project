FROM centos:7

WORKDIR /usr/src/

RUN yum install -y gcc openssl-devel bzip2-devel libffi-devel zlib-devel xz-devel make \
    && curl -o Python-3.7.11.tgz https://www.python.org/ftp/python/3.7.11/Python-3.7.11.tgz \
    && tar xzf Python-3.7.11.tgz \
    && rm Python-3.7.11.tgz \
    && cd Python-3.7.11 \
    && ./configure --enable-optimizations \
    && make altinstall \
    && yum remove -y gcc openssl-devel bzip2-devel libffi-devel zlib-devel xz-devel make \
    && ln -s /usr/local/bin/python3.7 /usr/bin/python3 \
    && ln -s /usr/local/bin/pip3.7 /usr/bin/pip3 \
    && pip3 install flask flask-jsonpify flask-restful \
    && mkdir /python_api

COPY python-api.py /python_api/python-api.py

CMD python3 /python_api/python-api.py